import React, {useState, useEffect} from 'react';
import {StatusBar} from 'react-native';
import SplashScreen from 'react-native-splash-screen';

//Codepush
import codePush from 'react-native-code-push';

//Redux
import configureStore from './app/stores';
const {persistor, store} = configureStore();

import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

// UI Framework
import {
  configureFonts,
  DefaultTheme,
  Provider as PaperProvider,
} from 'react-native-paper';

//Navigation
import Navigation from './app/navigations';
import {NavigationContainer} from '@react-navigation/native';

//utils
import {Colors, Fonts} from './app/utils';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.primary,
    accent: Colors.accent,
  },
  fonts: configureFonts(Fonts),
};
function App() {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <PaperProvider theme={theme}>
          <NavigationContainer>
            <StatusBar translucent backgroundColor="transparent" />
            <Navigation />
          </NavigationContainer>
        </PaperProvider>
      </PersistGate>
    </Provider>
  );
}
export default App;

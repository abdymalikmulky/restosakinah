export const SLIDE_DUMMY = [
  {
    title: 'Beautiful and dramatic Antelope Canyon',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
    url: 'https://www.instagram.com/p/B-6ccyQBEiR/',
    illustration: '',
  },
];

export const COUNTER_NEG_DEF = [
  {
    label: 'OTG',
    value: 0,
    valueProgress: 0,
    color: '#8e44ad',
  },
  {
    label: 'ODP',
    value: 0,
    valueProgress: -15,
    color: '#74CB00',
  },
  {
    label: 'PDP',
    value: 0,
    valueProgress: -3,
    color: '#FF7F00',
  },
];

export const COUNTER_POS_DEF = [
  {
    label: 'POSITIF',
    value: 0,
    valueProgress: 0,
    color: '#E40F12',
  },
  {
    label: 'SEMBUH',
    value: 0,
    valueProgress: 0,
    color: '#3ABABD',
  },
  {
    label: 'MENINGGAL',
    value: 0,
    valueProgress: 0,
    color: '#6E7774',
  },
];

export const HOTLINE = [
  {
    icon: 'phone-call',
    label: 'DINKES SULAWESI TENGGARA',
    number: '+62 811-4451-119',
  },
  {
    icon: 'phone-call',
    label: 'IDI (Ikatan Dokter Indonesia)',
    number: '+62 821-8743-3107',
  },
];

export const NEWS_DUMMY = [
  {
    title: 'Mahasiswa UHO Jalani Ujian Skripsi Online',
    date: '17 Apr 2020 09:50',
    image:
      'https://scontent-sin6-2.cdninstagram.com/v/t51.2885-15/e35/s240x240/93691654_245831166561531_7188998331020416838_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=103&_nc_ohc=5E9VSNDFo38AX-xzJrr&oh=455524b4959de059afdb36d85e42d539&oe=5EC2F4F3',
    author: 'kendariinfo',
  },
  {
    title:
      'Pemuda Muhamadiyah SULTRA Bagikan Paket Sembako Untuk Petugas Kebersihan',
    date: '17 Apr 2020 11:50',
    image:
      'https://scontent-sin6-1.cdninstagram.com/v/t51.2885-15/e35/s240x240/93809285_1069297566797056_4397586371745169637_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=104&_nc_ohc=09EPXuTAP_gAX_DKx9k&oh=1aac85145de55673743ca249d70cd1a5&oe=5EC14963',
    author: 'kendariinfo',
  },
  {
    title: 'Jokowi Yakin Corona Hilang Akhir Tahun, 2021 Pariwisata Booming',
    date: '17 Apr 2020 12:50',
    image:
      'https://scontent-sin6-1.cdninstagram.com/v/t51.2885-15/e15/s240x240/93391988_156139685781215_773195060679868954_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=111&_nc_ohc=3laD699pxFIAX9IouOO&oh=e563583a6ab6f94b0a56df53aa262ee7&oe=5EC36AB9',
    author: 'detik.com',
  },
];

export const MENU = [
  {
    id: 1,
    value: 'Aduan',
    label: 'Lapor Aduan',
    icon: require('../assets/images/menu-aduan.png'),
    active: true,
  },
  {
    id: 2,
    value: 'lapor-kasus',
    label: 'Lapor Kasus',
    icon: require('../assets/images/menu-kasus.png'),
    active: false,
  },
  {
    id: 3,
    value: 'wellness-center',
    label: 'Wellness Center',
    icon: require('../assets/images/menu-wc.png'),
    active: false,
  },
  {
    id: 4,
    value: 'survey',
    label: 'Survey',
    icon: require('../assets/images/menu-survey.png'),
    active: false,
  },
  {
    id: 5,
    value: 'check-in',
    label: 'Check-in',
    icon: require('../assets/images/menu-checkin.png'),
    active: false,
  },
  {
    id: 6,
    value: 'panic-button',
    label: 'Panic Button',
    icon: require('../assets/images/menu-panic.png'),
    active: false,
  },
];

const consts = {
  code_push: {
    staging: {
      ios: '19Rgqua2kaEig_E0ybqw2b3vv8JFeEdvjdX6v',
      android: 's9qq7-XsNV2qUo0bUMCQpagsBK8GTemRHiiAc',
    },
    prod: {
      ios: 'q0DoEqd_t4bR8UzsIFHD6hBGba6zK3DQSQdRY0',
      android: 'zx4pfQk9mkVP6vZK6q2N9SrO74Uu-yoJGE64Y2',
    },
  },
  accent: '#AD282A',
  white: '#fff',
  mute: '#ddd',
  placeholder: '#aaa',
  moreMute: '#888',
};
export default consts;

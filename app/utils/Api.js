const baseUrl = 'https://resto-sakinah.herokuapp.com/';
const api = {
  base_url: baseUrl + 'api/',
  image_base_url: baseUrl + 'uploads/',
  endpoint_food: 'food',
  endpoint_auth: 'login',
};
export default api;

const fontConfig = {
  default: {
    regular: {
      fontFamily: 'Poppins-Regular',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'Poppins-SemiBold',
      fontWeight: 'normal',
    },
    light: {
      fontFamily: 'Poppins-Light',
      fontWeight: 'normal',
    },
    italic: {
      fontFamily: 'Poppins-Italic',
      fontWeight: 'normal',
    },
  },
};
export default fontConfig;

const colors = {
  primary: '#d35400',
  accent: '#f39c12',
  danger: '#c0392b',
  white: '#fff',
  mute: '#ddd',
  placeholder: '#aaa',
  moreMute: '#888',
  textColor: '#333',
};
export default colors;

import React from 'react';
import {View, StyleSheet, StatusBar, Image, Dimensions} from 'react-native';
import {Text, Appbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Feather';

import {Colors} from '../../utils';

const deviceHeight = Dimensions.get('window').height;
const appLogo = require('../../assets/images/app_logo.png');

const Header = ({
  theme,
  overlapHeight,
  showBorder = true,
  navigation,
  isHome = true,
  title,
  subTitle,
}) => {
  let headerBorder = null;
  if (!showBorder) {
    headerBorder = {elevation: 0, shadowOpacity: 0, borderBottomWidth: 0};
  }

  const _goBack = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.white} barStyle={'dark-content'} />
      <Appbar.Header
        style={[
          {height: overlapHeight && deviceHeight * 0.2},
          styles.header,
          headerBorder,
        ]}>
        {isHome && (
          <Image source={appLogo} style={styles.appLogo} resizeMode="contain" />
        )}
        {isHome && (
          <Appbar.Content
            title="RNAPP"
            subtitle="RN Base App"
            style={styles.content}
            subtitleStyle={styles.subTitle}
          />
        )}

        {!isHome && <Appbar.BackAction onPress={_goBack} />}
        {!isHome && (
          <Appbar.Content
            title={title}
            subtitle={subTitle}
            style={styles.content}
            subtitleStyle={styles.subTitle}
          />
        )}
      </Appbar.Header>
    </View>
  );
};

export default Header;

const paddingHedaer = 4;
const styles = StyleSheet.create({
  container: {
    zIndex: 1,
    backgroundColor: Colors.white,
  },
  header: {
    marginTop: 26,
    marginBottom: 12,
    marginLeft: 12,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: Colors.white,
  },
  subTitle: {
    marginTop: -6,
  },
  notification: {
    marginTop: 0,
  },
  content: {
    paddingLeft: paddingHedaer - 2,
    paddingTop: paddingHedaer,
    borderBottomColor: '#f0f',
  },
  appLogo: {
    marginTop: paddingHedaer,
    marginLeft: paddingHedaer + 2,
    width: 55,
    height: 55,
  },
});

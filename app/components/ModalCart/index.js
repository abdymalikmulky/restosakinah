import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  FlatList,
  Image,
  Text,
  View,
  Dimensions,
} from 'react-native';
import {Button} from 'react-native-paper';
import Modal from 'react-native-modal';
import {Colors} from '../../utils';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const ModalCart = ({items, visible, toggleModal, onOrder}) => {
  const convertToRupiah = angka => {
    var rupiah = '';
    var angkarev = angka
      .toString()
      .split('')
      .reverse()
      .join('');
    for (var i = 0; i < angkarev.length; i++)
      if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
    return (
      'Rp. ' +
      rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('')
    );
  };

  const [total, setTotal] = useState(0);
  useEffect(() => {
    let totalPrice = 0;
    items.forEach(item => {
      if (item.food_buy > 0 && item.food_buy !== null) {
        totalPrice += item.food_buy * item.food_price;
      }
    });
    setTotal(totalPrice);
  }, [items]);

  const renderItem = ({item}) =>
    item.food_buy > 0 && item.food_buy !== null ? (
      <View style={styles.containerBox}>
        <Image source={{uri: `${item.food_image}`}} style={styles.image} />
        <View style={styles.content}>
          <Text style={styles.title} numberOfLines={2}>
            {item.food_name}
          </Text>
          <View style={styles.subContent}>
            <Text style={styles.price}>{convertToRupiah(item.food_price)}</Text>
            <Text style={styles.stock}>{item.food_buy} Item</Text>
          </View>
        </View>
      </View>
    ) : null;

  return (
    <Modal isVisible={visible}>
      <View style={styles.container}>
        <Text style={styles.title}>CART</Text>
        {items.length > 0 && (
          <View style={styles.content}>
            <FlatList
              style={styles.list}
              data={items}
              renderItem={renderItem}
              keyExtractor={item => item.id}
            />
          </View>
        )}

        <View style={{flexDirection: 'row'}}>
          <Button style={styles.action} onPress={toggleModal}>
            Close
          </Button>
          <Button color={Colors.accent} style={styles.order} onPress={onOrder}>
            Order
          </Button>
          <Text style={styles.total}>TOTAL: {convertToRupiah(total)}</Text>
        </View>
      </View>
    </Modal>
  );
};

export default ModalCart;

const styles = StyleSheet.create({
  containerBox: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 16,
    paddingRight: 20,
    borderTopColor: '#ddd',
    borderTopWidth: 0.5,
  },
  container: {
    alignItems: 'flex-start',
    backgroundColor: Colors.white,
    paddingVertical: 16,
    borderRadius: 12,
  },
  content: {
    maxHeight: deviceHeight / 2,
  },
  list: {width: deviceWidth / 1.2},
  subTitle: {
    fontSize: 14,
  },
  action: {
    marginTop: 12,
  },
  order: {
    marginTop: 12,
  },
  image: {
    width: 65,
    height: 65,
    borderRadius: 5,
    marginRight: 12,
  },
  title: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 18,
    alignSelf: 'center',
  },
  price: {
    marginTop: 5,
    fontSize: 16,
    color: '#333',
  },
  stock: {
    fontSize: 12,
    color: '#999',
  },
  total: {
    marginTop: 20,
    paddingLeft: 20,
    fontSize: 16,
    textAlign: 'right',
    alignItems: 'flex-end',
  },
});

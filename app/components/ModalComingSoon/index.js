import React from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import {Button} from 'react-native-paper';
import Modal from 'react-native-modal';
import {Colors} from '../../utils';

const deviceHeight = Dimensions.get('window').height;

const ModalComingSoon = ({visible, toggleModal}) => {
  return (
    <Modal isVisible={visible}>
      <View style={styles.container}>
        <Text style={styles.title}>Under Development Feature</Text>
        <Text style={styles.subTitle}>Coming Soon!</Text>
        <Button style={styles.action} onPress={toggleModal}>
          Close
        </Button>
      </View>
    </Modal>
  );
};

export default ModalComingSoon;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
    paddingVertical: 16,
    borderRadius: 12,
  },
  title: {
    fontSize: 16,
    marginBottom: 4,
  },
  subTitle: {
    fontSize: 14,
  },
  action: {
    marginTop: 12,
  },
});

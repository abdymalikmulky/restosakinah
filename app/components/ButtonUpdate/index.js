import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {Text, Button, Card} from 'react-native-paper';

//codepush
import codePush from 'react-native-code-push';
import {Colors, Consts} from '../../utils';

const ButtonUpdate = () => {
  const [appInfoUpdate, setAppInfoUpdate] = useState(null);
  const [isUpdating, setIsUpdating] = useState(false);

  useEffect(() => {
    codePush
      .checkForUpdate(Consts.code_push.prod.android, update => {
        console.log('handleBinaryVersionMismatchCallback: ', update);
      })
      .then(update => {
        console.log('Code push remote package: ', update);
        setAppInfoUpdate(update);
      });
  }, []);

  const onUpdateApp = () => {
    setIsUpdating(true);
    codePush
      .sync({
        updateDialog: true,
        installMode: codePush.InstallMode.IMMEDIATE,
      })
      .then(status => {
        setIsUpdating(false);
        if (status === 0) {
          //upto date
          alert('Your app already up to date');
        }
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.appVersionContainer}>
        <View style={styles.box}>
          <Card style={styles.counterTopContainer} elevation={2}>
            <Card.Content style={styles.counterContainer}>
              {appInfoUpdate === null ? (
                <>
                  <Text>App Version: Latest Version</Text>
                  <Button
                    style={styles.btnLogin}
                    mode="contained"
                    loading={isUpdating}
                    disabled={isUpdating}
                    onPress={onUpdateApp}>
                    CHECK FOR UPDATE
                  </Button>
                </>
              ) : (
                <>
                  <Button
                    style={styles.btnLogin}
                    mode="contained"
                    loading={isUpdating}
                    disabled={isUpdating}
                    onPress={onUpdateApp}>
                    NEW UPDATE APP
                  </Button>
                  <Text>
                    Update This App to Version {appInfoUpdate.appVersion}.
                    {appInfoUpdate.label}
                  </Text>
                </>
              )}
            </Card.Content>
          </Card>
        </View>
      </View>
    </View>
  );
};

export default ButtonUpdate;

const styles = StyleSheet.create({
  container: {
    zIndex: 1,
    backgroundColor: Colors.white,
  },
  header: {
    marginTop: 26,
    marginBottom: 12,
    marginLeft: 12,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: Colors.white,
  },
  subTitle: {
    marginTop: -6,
  },
  notification: {
    marginTop: 0,
  },
  counterContainer: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
});

import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import {Button} from 'react-native-paper';
import Modal from 'react-native-modal';
import {Colors} from '../../utils';

const CustomModal = ({visible, title, message, hideModal}) => {
  return (
    <Modal isVisible={visible}>
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subTitle}>{message}</Text>
        <Button
          style={styles.action}
          onPress={() => {
            hideModal();
          }}>
          Close
        </Button>
      </View>
    </Modal>
  );
};

export default CustomModal;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
    paddingVertical: 16,
    borderRadius: 12,
  },
  title: {
    fontSize: 16,
    marginBottom: 4,
  },
  subTitle: {
    fontSize: 14,
  },
  action: {
    marginTop: 12,
  },
});

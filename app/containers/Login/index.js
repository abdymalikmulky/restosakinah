import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  StatusBar,
  Keyboard,
} from 'react-native';
import {Text, TextInput, Button} from 'react-native-paper';
import {Modal} from '../../components';

//redux
import {useSelector, useDispatch, shallowEqual} from 'react-redux';
import {auth, signOut} from '../../stores/actions/auth';

import apiRequest from '../../stores/api/_base';

import {Api, Colors} from '../../utils';

import styles from './style';

const logo = require('../../assets/images/app_logo.png');

import axios from 'axios';

const Login = ({navigation, theme}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const {isAuthenticated, authAttemp, authError, user} = useSelector(
    state => ({
      authAttemp: state.user.authAttemp,
      isAuthenticated: state.user.isAuthenticated,
      authError: state.user.authError,
      user: state.user.user,
    }),
    shallowEqual,
  );
  const dispatch = useDispatch();
  useEffect(() => {
    console.log(
      'authError, authAttemp, isAuthenticated',
      authError,
      authAttemp,
      isAuthenticated,
    );
    if (authError !== '' && !authAttemp && !isAuthenticated) {
      setShowDialog(true);
    }
  }, [authError, authAttemp, isAuthenticated]);

  const [showDialog, setShowDialog] = useState(false);

  const onLogin = () => {
    dispatch(auth(username, password));
  };

  const hideModal = () => {
    dispatch(signOut());
    setShowDialog(false);
  };

  return (
    <>
      <StatusBar translucent={false} backgroundColor={Colors.accent} />
      <Modal
        visible={showDialog}
        title={'ERROR'}
        message={authError}
        hideModal={hideModal}
      />
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior={'position'} style={{flex: 1}}>
          <View style={styles.content}>
            <View style={styles.imageContent}>
              <Image
                style={styles.image}
                source={logo}
                resizeMode={'contain'}
              />
            </View>
            <View style={styles.formContent}>
              <TextInput
                style={styles.input}
                mode={'outlined'}
                label="Username"
                value={username}
                placeholder={'Username'}
                autoCapitalize={'none'}
                onChangeText={text => setUsername(text)}
                underlineColor={'#ff0'}
                returnKeyType={'next'}
              />
              <TextInput
                style={styles.input}
                secureTextEntry={true}
                mode={'outlined'}
                label="Password"
                value={password}
                placeholder={'Password'}
                autoCapitalize={'none'}
                onChangeText={text => setPassword(text)}
              />
            </View>
            <View style={styles.desc}>
              <Text style={styles.descText}>
                Login dengan akun anda{'\n'}Kontak admin jika belum memiliki
                akun
              </Text>
            </View>
          </View>
          <Button
            style={styles.btnLogin}
            mode="contained"
            loading={authAttemp}
            onPress={onLogin}>
            LOGIN
          </Button>
        </KeyboardAvoidingView>
      </ScrollView>
    </>
  );
};

export default Login;

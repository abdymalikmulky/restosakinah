import {Dimensions} from 'react-native';
import {Colors} from '../../utils';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {},
  imageContent: {},
  image: {
    width: deviceWidth / 2.5,
    height: deviceHeight / 2.25,
    alignSelf: 'center',
  },
  formContent: {
    alignSelf: 'center',
  },
  input: {
    width: deviceWidth / 1.2,
    marginBottom: 16,
    backgroundColor: Colors.white,
  },
  desc: {
    alignSelf: 'center',
  },
  descText: {
    textAlign: 'center',
    fontSize: 12,
    color: Colors.moreMute,
  },
  btnLogin: {
    marginTop: 22,
    width: deviceWidth / 1.2,
    alignSelf: 'center',
    backgroundColor: Colors.accent,
    borderRadius: 22,
  },
  background: {
    position: 'absolute',
    width: deviceWidth,
    bottom: -185,
    left: 0,
  },
};

export default styles;

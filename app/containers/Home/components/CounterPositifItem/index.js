import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Text, ActivityIndicator} from 'react-native-paper';
import {Colors} from '../../../../utils';
import Icon from 'react-native-vector-icons/FontAwesome';

const CounterPositifItem = ({item, index, isLoading}) => {
  let border = null;
  if (index === 1) {
    border = {
      borderLeftWidth: 1,
      borderLeftColor: '#ddd',
      borderRightWidth: 1,
      borderRightColor: '#ddd',
    };
  }

  return (
    <View style={[border, styles.counterConfirmDetailItem]}>
      <View style={styles.labelConfirmContainer}>
        <Text style={[{color: item.color}, styles.labelConfirm]}>
          {item.label}
        </Text>
      </View>
      <View style={styles.valueConfirmContainer}>
        {isLoading ? (
          <ActivityIndicator
            animating={true}
            color={Colors.primary}
            size={'small'}
            style={styles.loading}
          />
        ) : (
          <View style={styles.valueContent}>
            <Text style={styles.value}>{item.value}</Text>
            {item.valueProgress !== 0 && (
              <View style={styles.statContainer}>
                <Icon
                  name={item.valueProgress > 0 ? 'caret-up' : 'caret-down'}
                  color={item.color}
                  size={22}
                  style={styles.iconStat}
                />
                <Text style={styles.textStat}>{item.valueProgress}</Text>
              </View>
            )}
          </View>
        )}
      </View>
    </View>
  );
};

export default CounterPositifItem;

const styles = StyleSheet.create({
  valueContent: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  statContainer: {
    marginRight: -10,
    marginLeft: 4,

    flexDirection: 'row',
  },
  textStat: {
    fontSize: 8,
    marginTop: 6,
    marginLeft: 2,
    fontFamily: 'OpenSans-SemiBold',
  },
  iconStat: {},
  labelConfirm: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
    alignSelf: 'center',
    textAlign: 'center',
  },
  counterConfirmDetailItem: {
    flex: 1,
  },
  labelConfirmContainer: {
    paddingBottom: 2,
  },
  value: {
    color: Colors.primary,
    alignSelf: 'center',
    fontSize: 28,
    fontFamily: 'OpenSans-SemiBold',
  },
  loading: {
    marginTop: 8,
  },
});

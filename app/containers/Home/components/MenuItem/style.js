import Colors from '../../../../utils/Colors';

const React = require('react-native');
const {Dimensions, Platform} = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1,
    marginHorizontal: 5,
    marginBottom: 10,
    height: 120,
  },
  icon: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: 75,
    height: 75,
  },
  label: {
    alignSelf: 'center',
    textAlign: 'center',
    marginTop: 6,
    fontSize: 12,
    color: '#313450',
  },
  labelComingSoon: {
    fontSize: 10,
    color: Colors.placeholder,
    fontFamily: 'Poppins-Light',
    textAlign: 'center',
    fontStyle: 'italic',
  },
};

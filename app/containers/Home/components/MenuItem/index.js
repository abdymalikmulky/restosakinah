import React from 'react';
import {Image, View, TouchableOpacity} from 'react-native';
import {Text} from 'react-native-paper';

import styles from './style';

const MenuItem = ({customStyle, gridItem, onPress}) => {
  const item = gridItem.item;
  let opacity = 1;
  if (!item.active) {
    opacity = 0.6;
  }
  return (
    <View style={[customStyle, styles.container, {opacity: opacity}]}>
      <TouchableOpacity
        onPress={() => {
          onPress(item);
        }}>
        <Image source={item.icon} style={styles.icon} />
        <Text style={styles.label}>{item.label}</Text>
        {!item.active && (
          <Text style={styles.labelComingSoon}>(Coming Soon)</Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default MenuItem;

import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Text, Card, ActivityIndicator} from 'react-native-paper';
import {Colors} from '../../../../utils';
import Icon from 'react-native-vector-icons/FontAwesome';

const CounterItem = ({item, index, isLoading}) => {
  let valueProgress = item.valueProgress;
  if (valueProgress > 0) {
    valueProgress = '+' + valueProgress;
  }
  return (
    <Card style={styles.counterItem}>
      <Card.Content style={styles.counterContent}>
        <View style={[{backgroundColor: item.color}, styles.labelContainer]}>
          <Text style={styles.label}>{item.label}</Text>
        </View>
        <View style={styles.valueContainer}>
          {isLoading ? (
            <ActivityIndicator
              animating={true}
              color={Colors.primary}
              size={'small'}
              style={styles.loading}
            />
          ) : (
            <View style={styles.valueContent}>
              <Text style={styles.value}>{item.value}</Text>
              {item.valueProgress !== 0 && (
                <View style={styles.statContainer}>
                  <Icon
                    name={item.valueProgress > 0 ? 'caret-up' : 'caret-down'}
                    color={item.color}
                    size={22}
                    style={styles.iconStat}
                  />
                  <Text style={styles.textStat}>{valueProgress}</Text>
                </View>
              )}
            </View>
          )}
        </View>
      </Card.Content>
    </Card>
  );
};

export default CounterItem;

const styles = StyleSheet.create({
  valueContent: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  statContainer: {
    marginRight: -10,
    marginLeft: 4,

    flexDirection: 'row',
  },
  textStat: {
    fontSize: 10,
    marginTop: 4,
    marginLeft: 2,
    fontFamily: 'OpenSans-SemiBold',
  },
  iconStat: {},
  counterItem: {
    flex: 1,
    marginHorizontal: 4,
  },
  counterContent: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingHorizontal: 0,
  },
  labelContainer: {
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    paddingVertical: 6,
  },
  label: {
    color: 'white',
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
    alignSelf: 'center',
  },
  valueContainer: {
    paddingTop: 4,
    paddingBottom: 12,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
    borderTopWidth: 1,
    borderTopColor: '#ddd',
  },
  value: {
    color: Colors.primary,
    alignSelf: 'center',
    fontSize: 26,
    fontFamily: 'OpenSans-SemiBold',
  },
  loading: {
    marginTop: 8,
  },
});

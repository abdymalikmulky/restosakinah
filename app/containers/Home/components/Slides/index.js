import React from 'react';
import {StyleSheet, Image, View, Dimensions} from 'react-native';
import {TouchableRipple, Text} from 'react-native-paper';
import {Api} from '../../../../utils';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const Slides = ({item, onPress}) => {
  const convertToRupiah = angka => {
    var rupiah = '';
    var angkarev = angka
      .toString()
      .split('')
      .reverse()
      .join('');
    for (var i = 0; i < angkarev.length; i++)
      if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
    return (
      'Rp. ' +
      rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('')
    );
  };
  return (
    <TouchableRipple onPress={onPress} rippleColor={'#ccc'}>
      <View style={styles.slideItem}>
        <Image
          source={{uri: `${item.food_image}`}}
          style={[styles.slideImage]}
        />
        <Text style={styles.title}>
          {item.food_name} - {convertToRupiah(item.food_price)}
        </Text>
      </View>
    </TouchableRipple>
  );
};

export default Slides;

const styles = StyleSheet.create({
  slideItem: {
    backgroundColor: 'floralwhite',
    height: deviceHeight / 2.5,
    borderColor: '#666',
    borderWidth: 0.4,
  },
  slideImage: {
    flex: 1,
  },
  title: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    color: '#fff',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    paddingVertical: 6,
    paddingHorizontal: 10,
  },
});

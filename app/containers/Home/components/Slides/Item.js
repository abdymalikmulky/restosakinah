import React from 'react';
import {StyleSheet, Image, View, Dimensions} from 'react-native';
import {TouchableRipple, Text} from 'react-native-paper';
import {Api} from '../../../../utils';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const SlideItem = ({item, onPress}) => {
  return (
    <TouchableRipple onPress={onPress} rippleColor={'#ccc'}>
      <View style={styles.slideItem}>
        <Image
          source={{uri: `${item.news_image}`}}
          style={[styles.slideImage]}
        />
        <Text style={styles.title} numberOfLines={2}>
          {item.news_title}
        </Text>
      </View>
    </TouchableRipple>
  );
};

export default SlideItem;

const styles = StyleSheet.create({
  slideItem: {
    backgroundColor: 'floralwhite',
    height: deviceHeight / 4,
    borderColor: '#666',
    borderWidth: 0.4,
  },
  slideImage: {
    flex: 1,
  },
  title: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    color: '#fff',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    paddingVertical: 6,
    paddingHorizontal: 10,
  },
});

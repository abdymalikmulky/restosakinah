import React from 'react';
import {StyleSheet, Dimensions, View} from 'react-native';
import {Pagination} from 'react-native-snap-carousel';
import {Colors} from '../../../../utils';

const {width, height} = Dimensions.get('window');
const SlidePagination = ({slides, activeSlide}) => {
  return (
    <View style={styles.container}>
      <Pagination
        dotsLength={slides.length}
        activeDotIndex={activeSlide}
        containerStyle={styles.paginationContainer}
        dotStyle={styles.dot}
        inactiveDotStyle={styles.dotInactive}
        inactiveDotOpacity={1}
        inactiveDotScale={1}
      />
    </View>
  );
};

export default SlidePagination;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: -20,
    marginBottom: -35,
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: Colors.accent,
    borderColor: Colors.primary,
    borderWidth: 0.5,
  },
  dotInactive: {
    backgroundColor: Colors.moreMute,
    borderColor: Colors.mute,
    borderWidth: 0.5,
  },
});

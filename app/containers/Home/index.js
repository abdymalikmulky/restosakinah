import * as React from 'react';
import {
  View,
  SafeAreaView,
  Dimensions,
  Linking,
  RefreshControl,
  FlatList,
  ScrollView,
  Pressable,
  PlatformColor,
  Alert,
} from 'react-native';
import {
  ActivityIndicator,
  Text,
  Card,
  Button,
  TouchableRipple,
  FAB,
} from 'react-native-paper';

//redux
import {connect} from 'react-redux';
import {fetch, addToChart, reduceFromChart} from '../../stores/actions/food';

import Carousel from 'react-native-snap-carousel';

import Icon from 'react-native-vector-icons/Feather';
import {Header, ModalComingSoon, ModalCart} from '../../components';
import {Slides, SlidePagination, MenuItem} from './components';
import {FoodItem} from '../Food/components';
import {HOTLINE, SLIDE_DUMMY, MENU} from '../../utils/Data';
import {Colors, Api} from '../../utils';
import apiRequest from '../../stores/api/_base';
import styles from './style';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

let thisIsThis = null;
class Home extends React.Component {
  constructor(props) {
    super(props);
    thisIsThis = this;
    this.state = {
      user: null,
      showComingSoon: false,
      showBorder: false,
      showLoadingSlide: true,
      showLoadingContacts: true,
      showLoading: true,
      activeIndex: 0,
      refreshing: false,
      totalPositif: 0,
      slides: SLIDE_DUMMY,
      contacts: [],
      activeSlide: 0,
      showCart: false,
    };
  }

  componentDidMount() {
    const {user} = this.props.user;
    this.setState(user);
    this.props.fetch(user.token);
  }

  onScrolling = event => {
    const position = event.nativeEvent.contentOffset.y;
    if (position > 150) {
      this.setState({
        showBorder: true,
      });
    } else {
      this.setState({
        showBorder: false,
      });
    }
  };

  onRefresh = () => {
    const {user} = this.props.user;

    this.props.fetch(user.token);
  };

  toggleModal = () => {
    this.setState({
      showComingSoon: !this.state.showComingSoon,
    });
  };

  onAddToChart = item => {
    this.props.addToChart(item);
  };
  onReduceFromChart = item => {
    this.props.reduceFromChart(item);
  };

  _renderSlideItem({item, index}) {
    return (
      <Slides
        onPress={() => {
          alert(item.food_name);
        }}
        item={item}
      />
    );
  }

  toggleModalCart = () => {
    this.setState({
      showCart: !this.state.showCart,
    });
  };

  onOrder = () => {
    const {user} = this.props.user;
    this.props.fetch(user.token);
    alert('Thanks for ordering!');
    this.setState({
      showCart: false,
    });
  };
  onEdit = item => {
    const {user} = this.props.user;
    const {navigate} = this.props.navigation;
    navigate('FoodForm', {
      user,
      food: item,
      backHandler: this.onRefresh,
    });
  };
  onDel = async item => {
    Alert.alert('DELETE', 'Are you sure?', [
      {
        text: 'NO',
        onPress: () => {
          return true;
        },
      },
      {
        text: 'YES',
        onPress: () => {
          this.deleteItem(item);
        },
      },
    ]);
  };
  deleteItem = async item => {
    const {user} = this.props.user;

    try {
      const {data} = await apiRequest(
        `${Api.endpoint_food}/${item.resto_foods_id}/delete`,
        'get',
        null,
        null,
        user.token,
      );
      this.onRefresh();
    } catch (error) {
      console.log('errrror', error.response);
    }
  };
  onCreate = () => {
    const {user} = this.props.user;
    const {navigate} = this.props.navigation;
    navigate('FoodForm', {user, food: null, backHandler: this.onRefresh});
  };

  get pagination() {
    const {activeSlide} = this.state;
    const {list} = this.props.food;
    return <SlidePagination slides={list} activeSlide={activeSlide} />;
  }
  render() {
    const {refreshing, showCart, showComingSoon} = this.state;
    const {fetchAttemp, fetchError, list} = this.props.food;
    let {user} = this.props.user;
    if (!user) {
      user = {role: ''};
    }
    let totalCart = 0;
    list.forEach(itemFood => {
      if (itemFood.food_buy !== null) {
        totalCart += parseInt(itemFood.food_buy);
      }
    });
    return (
      <SafeAreaView style={styles.container}>
        {/* <Header showBorder={showBorder} navigation={this.props.navigation} /> */}
        <ModalComingSoon
          visible={showComingSoon}
          toggleModal={this.toggleModal}
        />
        <ModalCart
          items={list}
          visible={showCart}
          toggleModal={this.toggleModalCart}
          onOrder={this.onOrder}
        />
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.onRefresh}
            />
          }
          onScroll={this.onScrolling}>
          <View style={styles.slidesContainer}>
            <View style={styles.slidesContent}>
              {fetchAttemp ? (
                <ActivityIndicator
                  animating={true}
                  color={Colors.accent}
                  size={'small'}
                  style={styles.loading}
                />
              ) : (
                <>
                  <Carousel
                    style={styles.slides}
                    layout={'default'}
                    ref={ref => (this.carousel = ref)}
                    data={list}
                    firstItem={0}
                    sliderWidth={deviceWidth}
                    itemWidth={deviceWidth}
                    renderItem={this._renderSlideItem}
                    onSnapToItem={index => this.setState({activeSlide: index})}
                  />
                  {list.length > 0 && this.pagination}
                </>
              )}
            </View>
          </View>

          {/* package */}
          <Card style={styles.counterTopContainer} elevation={2}>
            <Card.Content style={styles.counterContainer}>
              <View style={styles.counterTitleContainer}>
                <Text style={styles.counterTitle}>Package</Text>
              </View>
              {fetchAttemp ? (
                <ActivityIndicator
                  animating={true}
                  color={Colors.primary}
                  size={'small'}
                  style={styles.loading}
                />
              ) : (
                <View style={styles.newsBox}>
                  {list
                    .filter(item => item.food_type === 'package')
                    .map(item => {
                      return (
                        <FoodItem
                          onPressDel={() => {
                            this.onDel(item);
                          }}
                          role={user && user.role}
                          onPressEdit={() => {
                            this.onEdit(item);
                          }}
                          onPress={() => {
                            this.onAddToChart(item);
                          }}
                          onPressReduce={() => {
                            this.onReduceFromChart(item);
                          }}
                          item={item}
                        />
                      );
                    })}
                </View>
              )}
            </Card.Content>
          </Card>

          {/* FOODS */}
          <Card style={styles.counterTopContainer} elevation={2}>
            <Card.Content style={styles.counterContainer}>
              <View style={styles.counterTitleContainer}>
                <Text style={styles.counterTitle}>Foods</Text>
              </View>
              {fetchAttemp ? (
                <ActivityIndicator
                  animating={true}
                  color={Colors.primary}
                  size={'small'}
                  style={styles.loading}
                />
              ) : (
                <View style={styles.newsBox}>
                  {list
                    .filter(item => item.food_type === 'food')
                    .map(item => {
                      return (
                        <FoodItem
                          role={user && user.role}
                          onPressDel={() => {
                            this.onDel(item);
                          }}
                          onPressEdit={() => {
                            this.onEdit(item);
                          }}
                          onPress={() => {
                            this.onAddToChart(item);
                          }}
                          onPressReduce={() => {
                            this.onReduceFromChart(item);
                          }}
                          item={item}
                        />
                      );
                    })}
                </View>
              )}
            </Card.Content>
          </Card>
          {/* DRINKS */}
          <Card
            style={[styles.counterTopContainer, {marginBottom: 80}]}
            elevation={2}>
            <Card.Content style={styles.counterContainer}>
              <View style={styles.counterTitleContainer}>
                <Text style={styles.counterTitle}>DRINK</Text>
              </View>
              {fetchAttemp ? (
                <ActivityIndicator
                  animating={true}
                  color={Colors.primary}
                  size={'small'}
                  style={styles.loading}
                />
              ) : (
                <View style={styles.newsBox}>
                  {list
                    .filter(item => item.food_type === 'drink')
                    .map(item => {
                      return (
                        <FoodItem
                          role={user && user.role}
                          onPressDel={() => {
                            this.onDel(item);
                          }}
                          onPressEdit={() => {
                            this.onEdit(item);
                          }}
                          onPress={() => {
                            this.onAddToChart(item);
                          }}
                          onPressReduce={() => {
                            this.onReduceFromChart(item);
                          }}
                          item={item}
                        />
                      );
                    })}
                </View>
              )}
            </Card.Content>
          </Card>
        </ScrollView>
        {!fetchAttemp && (
          <>
            {user.role === 'admin' ? (
              <FAB
                style={{
                  backgroundColor: Colors.primary,
                  position: 'absolute',
                  margin: 16,
                  right: 0,
                  bottom: 10,
                }}
                small
                color={Colors.white}
                icon="plus"
                label={`Create`}
                onPress={() => this.onCreate()}
              />
            ) : (
              <FAB
                style={{
                  position: 'absolute',
                  margin: 16,
                  right: 0,
                  bottom: 10,
                }}
                small
                color={Colors.white}
                icon="cart"
                label={`Cart (${totalCart})`}
                onPress={() => this.toggleModalCart()}
              />
            )}
          </>
        )}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    food: state.food,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    fetch: token => dispatch(fetch(token)),
    addToChart: food => dispatch(addToChart(food)),
    reduceFromChart: food => dispatch(reduceFromChart(food)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);

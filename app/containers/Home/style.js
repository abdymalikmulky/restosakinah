import {Colors} from '../../utils/';

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
  },
  slidesContainer: {},
  slides: {
    zIndex: 3,
  },
  slidesContent: {
    zIndex: 2,
  },
  counterTopContainer: {
    padding: 0,
    margin: 0,
    marginTop: 16,
  },
  counterContainer: {
    paddingBottom: 14,
    paddingHorizontal: 0,
    borderTopColor: '#fafafa',
    borderTopWidth: 1,
    backgroundColor: Colors.white,
  },
  counterBox: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 12,
  },
  counterTitleContainer: {
    marginHorizontal: 16,
    marginBottom: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  counterTitle: {
    color: Colors.textColor,
    fontFamily: 'Poppins-SemiBold',
    fontSize: 16,
    marginBottom: 8,
    paddingBottom: 0,
  },
  counterTitleDate: {
    fontSize: 12,
    paddingTop: 0,
    fontStyle: 'italic',
  },
  counterConfirmDetailContainer: {
    flexDirection: 'row',
  },
  counterConfirmContent: {
    paddingTop: 8,
    paddingLeft: 4,
    paddingRight: 4,
  },
  counterConfirmItem: {
    borderWidth: 0.2,
    borderColor: '#eee',
  },
  counterConfirmTitle: {
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
    paddingTop: 6,
    paddingBottom: 12,
    fontSize: 20,
  },
  counterConfirmBox: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 12,
  },
  btnPeta: {
    marginTop: 16,
    marginHorizontal: 16,
  },
  btnTxtPeta: {
    fontSize: 14,
    paddingVertical: 4,
  },
  hotlineContainer: {
    marginTop: 22,
  },
  hotlineCard: {
    marginHorizontal: 16,
    marginBottom: 12,
  },
  hotlineCardContent: {
    paddingLeft: 28,
    paddingVertical: 22,
    flexDirection: 'row',
  },
  hotlineLabel: {
    fontSize: 16,
  },
  hotlineIcon: {
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginRight: 16,
  },
  hotlineValue: {
    fontSize: 10,
    color: '#707070',
  },
  newsBox: {},
  loading: {
    paddingVertical: 16,
  },
  menuContainer: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
};

export default styles;

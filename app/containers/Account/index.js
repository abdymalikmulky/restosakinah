import React, {useState, useEffect} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {View, ScrollView} from 'react-native';
import {Text, Button, Card, ActivityIndicator} from 'react-native-paper';
import {Header} from '../../components';
import AsyncStorage from '@react-native-community/async-storage';

//redux
import {useSelector, useDispatch, shallowEqual} from 'react-redux';
import {signOut} from '../../stores/actions/auth';

import styles from './style';
import {Colors, Consts} from '../../utils';

const Account = ({navigation, theme}) => {
  const {user} = useSelector(
    state => ({
      user: state.user.user,
    }),
    shallowEqual,
  );
  const dispatch = useDispatch();

  const onLogout = async () => {
    dispatch(signOut());
  };

  return (
    <View style={styles.container}>
      <Header
        isHome={false}
        navigation={navigation}
        title={'Profile'}
        subTitle={'User Profile'}
      />
      <View style={styles.content}>
        {user !== null && (
          <View style={styles.box}>
            <Card style={styles.counterTopContainer} elevation={2}>
              <Card.Content style={styles.counterContainer}>
                <Text style={[styles.fullname, styles.title]}>
                  {user.username}
                </Text>
                <Text style={styles.fullname}>({user.role})</Text>
                <Text style={{fontSize: 12}}>{user.token}</Text>
              </Card.Content>
            </Card>
          </View>
        )}

        <Button
          style={styles.btnLogin}
          mode="contained"
          loading={false}
          onPress={onLogout}>
          SIGNOUT
        </Button>
      </View>
    </View>
  );
};

export default Account;

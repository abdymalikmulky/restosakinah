import React from 'react';
import {View} from 'react-native';
import {Text} from 'react-native-paper';
import {Header} from '../../components';

const Notification = ({navigation, theme}) => {
  return (
    <View>
      <Header />
      <Text>Notification</Text>
    </View>
  );
};

export default Notification;

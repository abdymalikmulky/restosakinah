const styles = {
  content: {
    padding: 16,
  },
  counterTopContainer: {
    marginBottom: 16,
  },
  counterContainer: {
    alignItems: 'center',
  },
  fullname: {
    textAlign: 'center',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 18,
  },
};

export default styles;

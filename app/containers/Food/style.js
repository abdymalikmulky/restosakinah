import {Dimensions} from 'react-native';
import {Colors} from '../../utils';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = {
  container: {},
  detail_image: {
    width: deviceWidth,
    height: 280,
    backgroundColor: Colors.mute,
  },
  detail_title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 18,
    paddingTop: 16,
    paddingHorizontal: 16,
  },
  detail_text_content: {
    paddingHorizontal: 16,
    paddingBottom: 16,
  },
  detail_date: {
    paddingHorizontal: 16,
    fontSize: 12,
  },
};

export default styles;

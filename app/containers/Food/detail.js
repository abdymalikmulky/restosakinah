import React, {useState, useEffect} from 'react';
import {View, ScrollView, Image} from 'react-native';
import {Text, Title, Button, Card} from 'react-native-paper';
import {Header} from '../../components';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import HTMLView from 'react-native-htmlview';

import styles from './style';
import {Api} from '../../utils';

const Detail = ({navigation, route}) => {
  const [user, setUser] = useState(null);
  const [news, setNews] = useState(route.params.news);

  useEffect(() => {
    async function fetchUser() {
      const userSession = await AsyncStorage.getItem('user');
      setUser(JSON.parse(userSession));
    }
    fetchUser();
  }, []);
  return (
    <ScrollView style={styles.container}>
      <Header
        isHome={false}
        navigation={navigation}
        title={'Berita'}
        subTitle={'Detail Berita'}
      />
      <View style={styles.detail_content}>
        <Image
          source={{uri: Api.image_base_url + news.news_image}}
          style={styles.detail_image}
          resizeMode="cover"
        />
        <Text style={styles.detail_title}>{news.news_title}</Text>
        <Text style={styles.detail_date}>{news.created_at}</Text>
        <HTMLView
          value={news.news_content}
          stylesheet={styles}
          style={styles.detail_text_content}
        />
      </View>
    </ScrollView>
  );
};

export default Detail;

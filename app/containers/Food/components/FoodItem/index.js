import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Text, TouchableRipple, Button} from 'react-native-paper';
import {Colors, Api} from '../../../../utils';

const FoodItem = ({
  role,
  onPress,
  onPressReduce,
  item,
  onPressEdit,
  onPressDel,
}) => {
  const isAdmin = role === 'admin';
  const convertToRupiah = angka => {
    var rupiah = '';
    var angkarev = angka
      .toString()
      .split('')
      .reverse()
      .join('');
    for (var i = 0; i < angkarev.length; i++)
      if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
    return (
      'Rp. ' +
      rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('')
    );
  };
  return (
    <TouchableRipple rippleColor={'#ccc'}>
      <View style={styles.container}>
        <Image source={{uri: `${item.food_image}`}} style={styles.image} />
        <View style={styles.content}>
          <Text style={styles.title} numberOfLines={2}>
            {item.food_name}
          </Text>
          <View style={styles.subContent}>
            <Text style={styles.price}>{convertToRupiah(item.food_price)}</Text>
            <Text style={styles.stock}>Stocks: {item.food_qty}</Text>
          </View>
        </View>
        <View style={styles.btnContainer}>
          {!isAdmin ? (
            <>
              <View style={{flexDirection: 'row'}}>
                <Button
                  style={[
                    styles.btnBuy,
                    {
                      backgroundColor:
                        item.food_qty === 0 ? null : Colors.accent,
                    },
                  ]}
                  mode={item.food_qty === 0 ? 'outlined' : 'contained'}
                  onPress={onPress}
                  disabled={item.food_qty === 0}>
                  Buy
                </Button>
                {item.food_buy !== null && item.food_buy > 0 && (
                  <Button
                    style={[
                      styles.btnCancel,
                      {
                        backgroundColor:
                          item.food_buy === null ? null : '#bdc3c7',
                      },
                    ]}
                    mode={'contained'}
                    onPress={onPressReduce}>
                    -
                  </Button>
                )}
              </View>
              {item.food_buy > 0 && (
                <Text style={styles.buy}>{item.food_buy} item</Text>
              )}
            </>
          ) : (
            <View style={{flexDirection: 'row'}}>
              <Button
                style={[styles.btnBuy, {backgroundColor: Colors.accent}]}
                mode={'contained'}
                onPress={onPressEdit}>
                Edit
              </Button>
              <Button
                style={[
                  styles.btnBuy,
                  {marginLeft: 4, backgroundColor: Colors.primary},
                ]}
                mode={'contained'}
                onPress={onPressDel}>
                Del
              </Button>
            </View>
          )}
        </View>
      </View>
    </TouchableRipple>
  );
};

export default FoodItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 16,
    paddingRight: 20,
    borderTopColor: '#ddd',
    borderTopWidth: 0.5,
  },
  btnBuy: {},
  btnCancel: {
    padding: 0,
    marginLeft: 5,
  },
  btnContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buy: {
    color: '#333',
    fontSize: 12,
  },
  image: {
    width: 65,
    height: 65,
    borderRadius: 5,
    marginRight: 12,
  },
  content: {
    flex: 1,
  },
  title: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 18,
  },
  price: {
    marginTop: 5,
    fontSize: 16,
    color: '#333',
  },
  stock: {
    fontSize: 12,
    color: '#999',
  },
});

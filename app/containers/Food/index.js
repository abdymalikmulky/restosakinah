import React from 'react';
import {View} from 'react-native';
import {Text} from 'react-native-paper';
import {Header} from '../../components';

const News = ({navigation, theme}) => {
  return (
    <View>
      <Header />
      <Text>News</Text>
    </View>
  );
};

export default News;

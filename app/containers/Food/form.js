import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Button, TextInput} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import {Header} from '../../components';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import apiRequest from '../../stores/api/_base';
import {Api} from '../../utils';
export default function Form({navigation, route}) {
  const [user, setUser] = useState(route.params.user);
  const [food, setFood] = useState(route.params.food);

  const [loading, setLoading] = useState(false);

  const [name, setName] = useState(food ? food.food_name : '');
  const [price, setPrice] = useState(food ? food.food_price : 0);
  const [type, setType] = useState(food ? food.food_type : '');
  const [qty, setQty] = useState(food ? food.food_qty : 0);
  const [urlImage, setUrlImage] = useState(food ? food.food_image : '');

  const onSubmit = async () => {
    try {
      setLoading(true);
      const request = {
        food_name: name,
        food_price: price,
        food_qty: qty,
        food_image: urlImage,
        food_type: type.toLowerCase(),
      };
      const {data} = await apiRequest(
        food
          ? `${Api.endpoint_food}/${food.resto_foods_id}/edit`
          : Api.endpoint_food,
        'post',
        request,
        null,
        user.token,
      );
      setLoading(false);
      route.params.backHandler();
      navigation.goBack();
    } catch (error) {
      setLoading(false);
      console.log('errrror', error.response);
    }
  };
  return (
    <View>
      <Header
        isHome={false}
        navigation={navigation}
        title={food !== null ? 'Edit' : 'Add'}
        subTitle={food ? food.food_name : 'Create New One'}
      />
      <View style={styles.form}>
        <TextInput
          style={styles.textInput}
          label="Enter Food Name"
          value={name}
          onChangeText={text => setName(text)}
        />
        <TextInput
          style={styles.textInput}
          label="Set Price"
          value={price}
          onChangeText={text => setPrice(text)}
        />
        <TextInput
          style={styles.textInput}
          label="Set Qty"
          value={qty}
          onChangeText={text => setQty(text)}
        />
        <TextInput
          style={styles.textInput}
          label="Enter Image URL"
          value={urlImage}
          onChangeText={text => setUrlImage(text)}
        />
        <TextInput
          style={styles.textInput}
          label="Set Type (Package, Food or Drink)"
          value={type}
          onChangeText={text => setType(text)}
        />
      </View>
      <Button
        style={styles.button}
        icon="plus"
        mode="contained"
        disabled={loading}
        onPress={() => onSubmit()}>
        {loading ? 'SUBMITING...' : 'SUBMIT'}
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  textInput: {
    backgroundColor: Colors.white,
  },
  button: {
    marginTop: 16,
  },
});

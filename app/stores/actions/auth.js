// index.js
import {auth as httpAuth} from '../api/auth';
import {AUTH, SIGN_OUT} from './types';

//LOGIN
export const auth = (username, password) => async dispatch => {
  dispatch({type: AUTH.ATTEMPT});

  try {
    const {data} = await httpAuth(username, password);
    const {success, message} = data;
    if (success) {
      dispatch(authSuccess(data.data));
    } else {
      dispatch(authFailed(message));
    }
  } catch (error) {
    console.log('errror');
    console.log('errror', error);
    dispatch(authFailed(error.response.data.message));
  }
};
export const authSuccess = user => {
  return {
    type: AUTH.SUCCESS,
    payload: user,
  };
};
export const authFailed = message => {
  return {
    type: AUTH.FAILED,
    message: message,
  };
};

//SIGN OUT
export const signOut = () => dispatch => {
  dispatch({type: SIGN_OUT.SUCCESS});
};

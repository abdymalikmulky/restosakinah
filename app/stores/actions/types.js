const generateType = type => ({
  ATTEMPT: `${type}_ATTEMPT`,
  SUCCESS: `${type}_SUCCESS`,
  FAILED: `${type}_FAILED`,
});

//AUTH
export const AUTH = generateType(`AUTH`);
export const SIGN_OUT = generateType(`SIGN_OUT`);

export const FETCH_FOOD = generateType(`FETCH_FOOD`);
export const ADD_TO_CHART = generateType(`ADD_TO_CHART`);
export const REDUCE_FROM_CHART = generateType(`REDUCE_FROM_CHART`);

// index.js
import {fetch as httpFetch} from '../api/food';
import {FETCH_FOOD, ADD_TO_CHART, REDUCE_FROM_CHART} from './types';

export const fetch = token => async (dispatch, getState) => {
  dispatch({type: FETCH_FOOD.ATTEMPT});
  try {
    const {data} = await httpFetch(token);
    const {success, message} = data;
    const list = data.data;
    if (success) {
      dispatch(fetchSuccess(list));
    } else {
      dispatch(fetchFailed(message));
    }
  } catch (error) {
    console.log('errr', error.response);
    dispatch(fetchFailed(error));
  }
};
export const fetchSuccess = (list, page) => {
  return {
    type: FETCH_FOOD.SUCCESS,
    payload: list,
    page: page,
  };
};
export const fetchFailed = message => {
  return {
    type: FETCH_FOOD.FAILED,
    message: message,
  };
};
export const addToChart = item => async (dispatch, getState) => {
  if (item.food_qty > 0) {
    dispatch(addToChartSuccess(item));
  } else {
    dispatch(addToChartFailed('Habis!!!'));
  }
};
export const addToChartSuccess = item => {
  return {
    type: ADD_TO_CHART.SUCCESS,
    payload: item,
  };
};
export const addToChartFailed = message => {
  return {
    type: ADD_TO_CHART.FAILED,
    message: message,
  };
};
export const reduceFromChart = item => async (dispatch, getState) => {
  if (item.food_buy > 0) {
    dispatch(reduceFromChartSuccess(item));
  } else {
    dispatch(reduceFromChartFailed('Habis!!!'));
  }
};
export const reduceFromChartSuccess = item => {
  return {
    type: REDUCE_FROM_CHART.SUCCESS,
    payload: item,
  };
};
export const reduceFromChartFailed = message => {
  return {
    type: REDUCE_FROM_CHART.FAILED,
    message: message,
  };
};

import {FETCH_FOOD, ADD_TO_CHART, REDUCE_FROM_CHART} from '../actions/types';

const defaultState = {
  fetchAttemp: false,
  fetchError: '',
  list: [],
};

export default function foodReducer(state = defaultState, action) {
  switch (action.type) {
    case FETCH_FOOD.ATTEMPT:
      return {
        ...state,
        fetchAttemp: true,
        fetchError: '',
      };
    case FETCH_FOOD.SUCCESS:
      return {
        ...state,
        fetchAttemp: false,
        list:
          state.list.length > 0 && action.page > 1
            ? state.list.concat(action.payload)
            : action.payload,
      };
    case FETCH_FOOD.FAILED:
      return {
        ...state,
        fetchAttemp: false,
        fetchError: action.message,
        list: [],
      };
    case ADD_TO_CHART.SUCCESS:
      let listFood = state.list;
      listFood = listFood.map(food => {
        return food.resto_foods_id === action.payload.resto_foods_id
          ? {
              ...food,
              food_qty: parseInt(action.payload.food_qty) - 1,
              food_buy:
                action.payload.food_buy === null
                  ? 1
                  : parseInt(action.payload.food_buy) + 1,
            }
          : food;
      });
      return {
        ...state,
        list: listFood,
      };
    case ADD_TO_CHART.FAILED:
      return {
        ...state,
        fetchError: action.message,
      };
    case REDUCE_FROM_CHART.SUCCESS:
      let listFoods = state.list;
      listFoods = listFoods.map(food => {
        return food.resto_foods_id === action.payload.resto_foods_id
          ? {
              ...food,
              food_qty: parseInt(action.payload.food_qty) + 1,
              food_buy:
                action.payload.food_buy === null
                  ? 1
                  : parseInt(action.payload.food_buy) - 1,
            }
          : food;
      });
      return {
        ...state,
        list: listFoods,
      };
    case REDUCE_FROM_CHART.FAILED:
      return {
        ...state,
        fetchError: action.message,
      };

    default:
      return state;
  }
}

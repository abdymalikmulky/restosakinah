import {combineReducers} from 'redux';

import food from './foodReducer';
import user from './userReducer';

export default combineReducers({
  food,
  user,
});

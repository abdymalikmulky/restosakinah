import {AUTH, SIGN_OUT} from '../actions/types';

const defaultState = {
  authAttemp: false,
  isAuthenticated: false,
  authError: '',
  user: null,
};

export default function userReducer(state = defaultState, action) {
  switch (action.type) {
    case AUTH.ATTEMPT:
      return {
        ...state,
        authAttemp: true,
        isAuthenticated: false,
        authError: '',
        user: null,
      };
    case AUTH.SUCCESS:
      return {
        ...state,
        authAttemp: false,
        isAuthenticated: true,
        authError: '',
        user: action.payload,
      };
    case AUTH.FAILED:
      return {
        ...state,
        authAttemp: false,
        isAuthenticated: false,
        authError: action.message,
        user: null,
      };
    case SIGN_OUT.SUCCESS:
      return {
        ...defaultState,
      };
    default:
      return state;
  }
}

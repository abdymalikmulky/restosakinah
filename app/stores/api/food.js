import apiRequest from './_base';
import {Api} from '../../utils';

const fetch = token => {
  const url = `${Api.endpoint_food}`;
  return apiRequest(`${url}`, 'get', null, null, token);
};
export {fetch};

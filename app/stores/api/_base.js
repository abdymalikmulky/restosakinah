import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {Api} from '../../utils';

const apiBaseUrl = Api.base_url;
export default (url, method, data, formData = null, token = '') => {
  return call(url, method, data, (formData = null), token);
};

const call = (url, method, data, formData = null, token = '') => {
  if (typeof method !== 'string') {
    throw new Error('Method must be a string!');
  }

  if (data !== undefined && typeof data !== 'object') {
    throw new Error('Data must be an object!');
  }

  let dataRequest =
    ['put', 'post', 'patch'].includes(method.toLowerCase()) && data;
  if (formData != null) {
    dataRequest = formData;
  }

  let dataHeaders = {
    'Content-Type': 'application/x-www-form-urlencoded',
    Token: token,
  };
  if (token !== '') {
    dataHeaders.Authorization = `Bearer ${token}`;
  }

  const axiosConfig = {
    method,
    url: apiBaseUrl + url,
    data: dataRequest,
    headers: dataHeaders,
    params: method.toLowerCase() === 'get' && data,
  };
  console.log('API-Request', axiosConfig);
  return axios(axiosConfig);
};

const getUser = () => {
  try {
    const value = AsyncStorage.getItem('persist:root');
    if (value !== null) {
      return value;
    } else {
      return false;
    }
  } catch (e) {
    return e;
  }
};

const getAllKeys = async () => {
  let keys = [];
  try {
    keys = await AsyncStorage.getAllKeys();
  } catch (e) {
    // read key error
  }

  console.log('ALLJKEYS', keys);
  // example console.log result:
  // ['@MyApp_user', '@MyApp_key']
};

import apiRequest from './_base';
import {Api} from '../../utils';

const auth = (username, password) =>
  apiRequest(Api.endpoint_auth, 'post', {
    username,
    password,
  });

export {auth};

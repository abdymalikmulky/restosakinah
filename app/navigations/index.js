import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector, shallowEqual} from 'react-redux';

//navigation
import TabNavigator from './components/TabNavigator';

//containers
import Home from '../containers/Home';
import Account from '../containers/Account';
import Login from '../containers/Login';
import Detail from '../containers/Food/detail';
import Food from '../containers/Food';
import FoodForm from '../containers/Food/form';
import Notification from '../containers/Notification';

const Stack = createStackNavigator();

const tabItems = [
  {
    name: 'Beranda',
    iconName: 'home',
    iconSize: 0,
    component: Home,
  },
  {
    name: 'Notification',
    iconName: 'notification',
    iconSize: 0,
    component: Notification,
  },
  {
    name: 'Account',
    iconName: 'user',
    iconSize: 0,
    component: Account,
  },
];
function StackNavigator() {
  const {isAuthenticated} = useSelector(
    state => ({
      isAuthenticated: state.user.isAuthenticated,
    }),
    shallowEqual,
  );
  return (
    <Stack.Navigator headerMode={'none'}>
      <>
        {!isAuthenticated ? (
          <>
            <Stack.Screen name="Login" component={Login} />
          </>
        ) : (
          <>
            <Stack.Screen name="Home" component={HomeTabs} />
            <Stack.Screen name="Account" component={Account} />
            <Stack.Screen name="Food" component={Food} />
            <Stack.Screen name="FoodForm" component={FoodForm} />
            <Stack.Screen name="NewsDetail" component={Detail} />
          </>
        )}
      </>
    </Stack.Navigator>
  );
}
function HomeTabs() {
  return <TabNavigator tabItems={tabItems} />;
}

export default StackNavigator;

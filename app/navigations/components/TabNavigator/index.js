import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/Entypo';

const Tab = createMaterialBottomTabNavigator();

const TabNavigator = ({tabItems}) => {
  return (
    <Tab.Navigator
      initialRouteName="Beranda"
      activeColor="#102344"
      inactiveColor="#aaa"
      barStyle={{backgroundColor: '#fff'}}>
      {tabItems.map(item => {
        return (
          <Tab.Screen
            key={item.name}
            name={item.name}
            component={item.component}
            options={{
              tabBarLabel: item.name,
              tabBarIcon: ({color}) => (
                <Icon
                  name={item.iconName}
                  color={color}
                  size={item.iconSize == 0 ? 22 : item.iconSize}
                />
              ),
            }}
          />
        );
      })}
    </Tab.Navigator>
  );
};

export default TabNavigator;

const styles = StyleSheet.create({});
